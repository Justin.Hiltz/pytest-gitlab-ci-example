from example_module.math import add, subtract


def test_add_positives():
    assert add(3, 4) == 7


def test_add_negatives():
    assert add(-2, -3) == -5


def test_add_mixed():
    assert add(2, -3) == -1


def test_add_zeros():
    assert add(0, 0) == 0


def test_subtract_positives():
    assert subtract(3, 4) == -1


def test_subtract_negatives():
    assert subtract(-2, -3) == 1


def test_subtract_mixed():
    assert subtract(2, -3) == 5


def test_subtract_zeros():
    assert subtract(0, 0) == 0
